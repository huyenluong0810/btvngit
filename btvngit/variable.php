<html>
    <head>
        <title> Phạm vi biến   </title>
</head>
<body>
    <?php 
    //$x=10; // Biến global - Biến toàn cục 
    //function myTest(){
        // $x =25; //Biến local
        //global $x; 
        //static $b = 12;
        //echo $x;
        //$b = $b +1 ;
        //echo 'Giá trị b= ', $b . '<br>';
    //}
    //myTest(); // b=13
    //myTest(); // b=14
    //function mySample($y) {
        //static $c=15;
        //echo $c + $y;
    //}
    //mySample(5); // Hiển thị 20

    //Cách 1 : Tự động chuyển kiểu
    //$var = "100" + 15;
    //$var1 = "100" + 15.0;
    //$var2 = 39 . "Step";
    //$var3 = "100" + (string) 15;
    // echo $var . '<br>';
    // echo $var1 . '<br>';
    // echo $var2 . '<br>';
    // echo $var3 . '<br>';
    // echo var_dump($var3);
    // echo var_dump($var1);

    // echo (abs(10).'<br>');
    // echo (abs(-10).'<br>');
    // echo (abs(-60.0).'<br>'); //Hàm giá trị tuyệt đối
    // echo (pow(2,3).'<br>'); //Hàm mũ 2^3 = 8
    // echo (ceil(0.6).'<br>'); //1 hàm làm tròn lên trên
    // echo (ceil(7.2).'<br>'); //8
    // echo (sqrt(9).'<br>'); //3 hàm căn bậc 2
    // echo (sqrt(-9).'<br>'); //NAN
    // echo (floor(0.6).'<br>'); // 0 hàm làm tròn xuống
    // echo (floor(5.1).'<br>'); // 5
    // echo (floor(-5.9).'<br>'); //-6
    // echo (log(2).'<br>'); //log(2)
    // echo (round(0.6).'<br>'); //1
    // echo (round(0.49).'<br>'); //0
    // echo decbin("2") .'<br>'; //convert số thập phân thành nhị phân 10
    // echo bindec("0011") .'<br>'; //convert nhị phân thành thập phân =>3
    // echo rand(1,10) .'<br>'; //lấy ngẫu nhiên từ 1-10

    //toán tử
    // $x = 10;
    // $y = 6;
    // echo $x % $y .'<br>'; //4 lấy phần dư
    // echo $x / $y .'<br>'; //1.66
    // echo $x ** $y .'<br>'; //1.66
    // echo $x ++ .'<br>'; //11
    // echo $x -- .'<br>'; //9
    // $x *=2;
    // echo $x  .'<br>';

    //Câu lệnh điều kiện if
    // $number = rand(0,1);
    // if ($number == 0){
    //     echo $number . " Là số 0";
    // } else{
    //     echo $number . "là lớn hơn 0";
    // }

    //câu lệnh switch
    // $day =9;
    // switch($day){
    //     case 2:
    //         echo "Thứ 2";
    //         break;
    //         case 3:
    //             echo "Thứ 3";
    //             break;
    //             case 4:
    //                 echo "Thứ 4";
    //                 break;
    //                 case 5:
    //                     echo "Thứ 5";
    //                     break;
    //                     case 6:
    //                         echo "Thứ 6";
    //                         break;
    //                         case 7:
    //                             echo "Thứ 7";
    //                             break;
    //                             case 8:
    //                                 echo "Chủ nhật";
    //                                 break;
    //     default:
    //     echo "Không thuộc thứ nào trong tuần";                            
    // }

    // while / do..while
    // $i = 1;
    // while($i<=5){
    //     echo "Số là : $i <br>";
    //     $i++;
    // }
    // echo "-----------------<br>";
    // $j = 1;
    // do{
    //     echo "Số là : $j <br>";
    //     $j++;
    // } while($j<=5);

    //vòng lặp for
    // for($i=1; $i<=10; $i++){
    //     echo "Hello Việt Nam <br>";
    // }

    //in ra tất cả các số chia hết cho 2
    // for($i=1; $i<=100; $i++){
    //     if ($i % 2 == 0) {
    //         echo "$i  ";
    //     }
    // }

    //tính tổng tất cả các số chẵn
    // $sum = 0;
    // for($i=1; $i<=100; $i++){
    //     if ($i % 2 == 0) {
    //         echo "$i ";
    //         $sum = $sum + $i; //$sum += $i
    //     }
    // }
    // echo " <br> Tổng số chẵn là : " .$sum;

    //tìm các số chia hết cho 5 và 3
//     for($i = 1; $i <=100; $i++){
// if ($i % 5 == 0 && $i % 3 == 0){
//     echo "$i <br>";
// }
//     }

//foreach
// $arr = array("Apple", "orange", "banana", "kiwi", "mango");
// foreach($arr as $value){
//     echo "Tên quả là " . $value . "<br>";
// }

//break continue
for($i=1; $i<=6; $i++){
    if ($i == 3) {
       // break; dừng lại
       continue;
    }
    echo "$i <br>";
}

    ?> 

</body>
</html>